export const Status={
  "Submitted":{"color":["#006400","#e8f5ec"]},
  "Due":{"color":["#ff0000","#faedeb"]},
  "Draft":{"color":["#63a0c4","#defcff"]},
  "Rejected":{"color":["#ff0000","#faedeb"]},
  "Tag Name":{"color":["#63a0c4","#defcff"]},
  "Raised":{"color":["#FF5733","#FFD580"]},
  "Paid":{"color":["#006400","#e8f5ec"]},
}