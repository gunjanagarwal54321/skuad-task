export const Category={
  "Travel":{"color":["#5a50a9","#f1f0ff"],"icon":<i class="fas fa-plane-departure"></i>},
  "Meals":{"color":["#e20e6b","#fdeef8"],"icon":<i class="fas fa-utensils"></i>},
  "Devices & Software":{"color":["#76bcac","#dbfaf6"],"icon":<i class="fas fa-tablet-alt"></i>},
  "Hotel":{"color":["#549dcd","#e9f7fe"],"icon":<i class="fas fa-hotel"></i>},
  "Other":{"color":["#cf875a","#ffefe5"],"icon":<i class="fas fa-arrows-alt"></i>}
}
    