import React, { useState} from 'react';
import '../styles.css'
import Pagination from './Pagination';
import RenderRow from './RenderRow';
export default function Table(props){
  const [currentPage, setCurrentPage] = useState(1);
  const [PerPage] = useState(10);
  const indexOfLast = currentPage * PerPage;
  const indexOfFirst = indexOfLast - PerPage;
  const current = props.data.slice(indexOfFirst, indexOfLast);

  const paginate = pageNumber => setCurrentPage(pageNumber);

  function getKeys(){
    return Object.keys(props.data[0]);
  }
  
  function getStatus(key){
    if(key==='Kebab' || key==='Download' || key==='Eye'){
      return 'none';
    }
  }

  function getHeader(){
    var keys = getKeys();
    return keys.map((key, index)=>{
      return (
      <th key={key} style={{'display':getStatus(key)}}>
        {key==='checkbox'?<input type="checkbox"></input>:key}
      </th>
      )
    })
  }
  
  function getRowsData(){
    var items = [];
    items=current;
    var keys = getKeys();
    return items.map((row, index)=>{
      return <tr key={index}><RenderRow key={index} data={row} keys={keys}/></tr>
    })
  }
    
  return (
    <div>
      <table>
      <thead>
        <tr>{getHeader()}</tr>
      </thead>
      <tbody>
        {getRowsData()}
      </tbody>
      </table>
      <Pagination PerPage={PerPage} total={props.data.length}  paginate={paginate}/>
    </div> 
  );
}


