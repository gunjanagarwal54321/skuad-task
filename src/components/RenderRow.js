import React, { useState} from 'react';
import { Category } from '../data/category';
import { Status } from '../data/status';

const RenderRow = (props) =>{
  const [isLoggedIn, setLoggedIn] = useState(true);

  const getCategory = function(data){
    return<div style={{color:Category[data].color[0],background:Category[data].color[1]}} className="category"><p>{Category[data].icon}&nbsp;&nbsp;{data}</p></div>
  }

  const getStatus = function(data){
    return<div style={{color:Status[data].color[0],background:Status[data].color[1]}} className="category"><p>{data}</p></div>
  }

  const getSpender = function(data){
    var randomColor = Math.floor(Math.random()*16777215).toString(16);
    return <div className="spender">
      <div className="initials" style={{"background":'#'+randomColor}}>{data[0]}</div>
      <div className="details">{data[1]} <br/> <span>{data[2]}</span></div>
    </div>
  }

  const getVendor = function(data){
    return <div className="details">{data[0]} <br/> <span>{data[1]}</span></div>
  }

  const handleLoginClick = function(isLoggedIn){
    setLoggedIn(!isLoggedIn)
  }

  const handleEvent = function(){
   return <div>
      {isLoggedIn ? (
        <div onClick= {() => {handleLoginClick(isLoggedIn)}}><i class="fas fa-ellipsis-v"></i></div>
      ) : (
        <div onClick= {() => {handleLoginClick(isLoggedIn)}}><i class="fas fa-ellipsis-v"></i><div className="Pay"><p>Pay Now</p><hr/><p>Reject</p></div></div>
      )}
    </div>
  }
  
  const getData = function(data,key){
    switch(key){
      case 'Kebab':
        return handleEvent()
      case 'Download':
        return <i class="fa fa-download"></i>
      case 'Eye':
        return <i class="fas fa-eye"></i>
      case 'checkbox':
        return <input type="checkbox"></input>
      case 'Category':
        return getCategory(data)  
      case 'Status':
        return getStatus(data)  
      case 'Spender':
        return getSpender(data) 
      case 'Vendor':
        return getVendor(data)  
      case 'Paid Amount':
        return getVendor(data)   
      default:
        return data ;       
    } 
  }
  return props.keys.map((key, index)=>{
    return <td key={props.data[key]}>{getData(props.data[key],key)}</td>
  })
}

export default RenderRow;